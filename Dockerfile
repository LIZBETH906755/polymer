# Imagen base
FROM node:latest

#Directorio de la app
WORKDIR /app

#Copia de archivos de origen a destino
ADD /build/default /app/build/default
ADD server.js /app
ADD package.json /app


#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 4006

#Comando para ejecutarse
CMD ["npm", "start"]
